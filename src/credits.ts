export class Credits extends Entity {
  constructor() {
    super()
    this.addComponent(new Transform({
      position: new Vector3(22.62, -0.14, 7.33),
      rotation: Quaternion.Euler(0, 132, 0),
      scale: new Vector3(1.2, 0.1, 1.8)
    }))
    const material = new Material()
    material.albedoTexture = new Texture("images/credits.png")
    this.addComponent(material)
    const creditsBox = new BoxShape()
    this.addComponent(creditsBox)
  }
}
