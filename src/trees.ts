export function loadTrees() {
  let tree1 = new Entity()
  tree1.addComponent(new GLTFShape("models/trees/dry_tree_colliders_mod2.gltf"))
  tree1.addComponent(new Transform({
    position: new Vector3(20, -0.15, 18.5),
    rotation: Quaternion.Euler(-2, -13, -5),
    scale: new Vector3(0.5, 0.5, 0.5)
  }))
  engine.addEntity(tree1)
  
  let tree2 = new Entity()
  tree2.addComponent(new GLTFShape("models/trees/oak_tree_colliders_mod2.gltf"))
  tree2.addComponent(new Transform({
    position: new Vector3(12, -0.4, 12.4),
    rotation: Quaternion.Euler(-27, -60, -15),
    scale: new Vector3(0.5, 0.5, 0.5)
  }))
  engine.addEntity(tree2)

  let tree3 = new Entity()
  tree3.addComponent(new GLTFShape("models/trees/ash_tree_collider.glb"))
  tree3.addComponent(new Transform({
    position: new Vector3(7, 0.35, 22),
    rotation: Quaternion.Euler(-32, 80, 39),
    scale: new Vector3(0.5, 0.5, 0.5)
  }))
  engine.addEntity(tree3)

  let tree4 = new Entity()
  tree4.addComponent(new GLTFShape("models/trees/aspen_tree_collider.gltf"))
  tree4.addComponent(new Transform({
    position: new Vector3(8.9, 0.2, 9.2),
    rotation: Quaternion.Euler(-27, -60, -15),
    scale: new Vector3(0.5, 0.5, 0.5)
  }))
  engine.addEntity(tree4)

  let tree5 = new Entity()
  tree5.addComponent(new GLTFShape("models/trees/ash_tree_collider.glb"))
  tree5.addComponent(new Transform({
    position: new Vector3(12, 0, 3),
    rotation: Quaternion.Euler(-7, 30, 11),
    scale: new Vector3(0.6, 0.7, 0.6)
  }))
  engine.addEntity(tree5)

  let tree6 = new Entity()
  tree6.addComponent(new GLTFShape("models/trees/oak_tree_colliders_mod2.gltf"))
  tree6.addComponent(new Transform({
    position: new Vector3(23.7, -0.8, 25.1),
    rotation: Quaternion.Euler(-1, -34, 4),
    scale: new Vector3(0.6, 0.5, 0.5)
  }))
  engine.addEntity(tree6)

  let tree7 = new Entity()
  tree7.addComponent(new GLTFShape("models/trees/ash_tree_collider.glb"))
  tree7.addComponent(new Transform({
    position: new Vector3(28.22, -0.17, 21.93),
    rotation: Quaternion.Euler(171, -58, 168),
    scale: new Vector3(0.66, 0.45, 0.65)
  }))
  engine.addEntity(tree7)

  let tree8 = new Entity()
  tree8.addComponent(new GLTFShape("models/trees/aspen_tree_collider.gltf"))
  tree8.addComponent(new Transform({
    position: new Vector3(14.0, 0.0, 27.2),
    rotation: Quaternion.Euler(9, -10, 8),
    scale: new Vector3(0.7, 0.8, 0.7)
  }))
  engine.addEntity(tree8)

  let tree9 = new Entity()
  tree9.addComponent(new GLTFShape("models/trees/aspen_tree_collider.gltf"))
  tree9.addComponent(new Transform({
    position: new Vector3(4.5, -0.2, 29.2),
    rotation: Quaternion.Euler(-6, 50, 8),
    scale: new Vector3(0.7, 0.6, 0.7)
  }))
  engine.addEntity(tree9)

  let tree10 = new Entity()
  tree10.addComponent(new GLTFShape("models/trees/oak_tree_colliders_mod2.gltf"))
  tree10.addComponent(new Transform({
    position: new Vector3(4.5, -0.5, 12.2),
    rotation: Quaternion.Euler(-170, 18, 180),
    scale: new Vector3(0.4, 0.3, 0.4)
  }))
  engine.addEntity(tree10)

  let tree11 = new Entity()
  tree11.addComponent(new GLTFShape("models/trees/aspen_tree_collider.gltf"))
  tree11.addComponent(new Transform({
    position: new Vector3(3, -0.2, 2.2),
    rotation: Quaternion.Euler(4, -82, -2),
    scale: new Vector3(0.6, 0.7, 0.7)
  }))
  engine.addEntity(tree11)

  let bush1 = new Entity()
  bush1.addComponent(new GLTFShape("models/trees/tree_collider.gltf"))
  bush1.addComponent(new Transform({
    position: new Vector3(24, -0.4, 11.5),
    rotation: Quaternion.Euler(-27, -60, -15),
    scale: new Vector3(1, 1, 1)
  }))
  engine.addEntity(bush1)

  let bush2 = new Entity()
  bush2.addComponent(new GLTFShape("models/trees/tree_collider.gltf"))
  bush2.addComponent(new Transform({
    position: new Vector3(10.2, -0.5, 26),
    rotation: Quaternion.Euler(176, -92, 176),
    scale: new Vector3(1, 1, 1)
  }))
  engine.addEntity(bush2)
} 
