enum Type {
  None,
  Main,
  Pool,
  Message
}

const progressBar = [
  [17, 363],
  [503, 104],
  [503, 190],
  [503, 277],
  [503, 363],
  [503, 453],
  [17, 453]
]

export class UI {
  static readonly Type = Type
  private canvas: UICanvas
  private mainUI: UIContainerRect
  private poolUI: UIContainerRect
  private messageUI: UIContainerRect
  private startButton: UIImage
  private amount: UIText
  private donated: UIText
  private poolProgress: UIImage
  private poolProgressCounter: number = 0
  private messageText: UIText
  active: Type = Type.None
  constructor() {
    this.canvas = new UICanvas()

    const imageAtlas = "images/ui.png"
    const imageTexture = new Texture(imageAtlas)

    this.poolUI = new UIContainerRect(this.canvas)
    this.poolUI.color = Color4.Black()
    this.poolUI.opacity = 0.6
    this.poolUI.adaptHeight = true  // one day this will work
    this.poolUI.adaptWidth = true
    this.poolUI.height = '15%'
    this.poolUI.width = '90%'
    this.poolUI.vAlign = 'middle'
    this.poolUI.hAlign = 'center'
    this.poolUI.visible = false

    const poolText = new UIText(this.poolUI)
    poolText.fontSize = 35
    poolText.value = 'The pool has cool and cleanest water'
    poolText.positionX = -230
    poolText.positionY = 60

    this.poolProgress = new UIImage(this.poolUI, imageTexture)
    this.poolProgress.sourceLeft = 17
    this.poolProgress.sourceTop = 363
    this.poolProgress.sourceWidth = 478
    this.poolProgress.sourceHeight = 75
    this.poolProgress.width = 400
    this.poolProgress.height = 32
    this.poolProgress.positionX = 0
    this.poolProgress.positionY = -20

    this.messageUI = new UIContainerRect(this.canvas)
    this.messageUI.color = Color4.Black()
    this.messageUI.opacity = 0.6
    this.messageUI.adaptHeight = true  // one day this will work
    this.messageUI.adaptWidth = true
    this.messageUI.height = '10%'
    this.messageUI.width = '90%'
    this.messageUI.vAlign = 'middle'
    this.messageUI.hAlign = 'center'
    this.messageUI.visible = false

    this.messageText = new UIText(this.messageUI)
    this.messageText.fontSize = 35
    this.messageText.value = ''
    this.messageText.positionX = -300
    this.messageText.positionY = 30

    this.mainUI = new UIContainerRect(this.canvas)
    this.mainUI.color = Color4.Black()
    this.mainUI.opacity = 0.6
    this.mainUI.adaptHeight = true  // one day this will work
    this.mainUI.adaptWidth = true
    this.mainUI.height = '40%'
    this.mainUI.width = '90%'
    this.mainUI.vAlign = 'middle'
    this.mainUI.hAlign = 'center'
    this.mainUI.visible = false

    const message = new UIText(this.mainUI)
    message.value = 'Woods need MANA to grow'
    message.fontSize = 35
    message.paddingTop = -20
    message.paddingLeft = 20
    message.paddingRight = 20
    message.positionX = -185
    message.positionY = 130

    const label1 = new UIText(this.mainUI)
    label1.value = 'Donate                MANA to help the Forest'
    label1.fontSize = 30
    label1.paddingLeft = 20
    label1.paddingRight = 20
    label1.positionX = -255
    label1.positionY = 70

    this.amount = new UIText(this.mainUI)
    this.amount.value = '100'
    this.amount.fontSize = 30
    this.amount.positionX = -105
    this.amount.positionY = 70

    const upButton = new UIImage(this.mainUI, imageTexture)
    upButton.sourceLeft = 179
    upButton.sourceTop = 278
    upButton.sourceWidth = 72
    upButton.sourceHeight = 72
    upButton.width = 30
    upButton.height = 16
    upButton.positionX = -80
    upButton.positionY = 46
    upButton.onClick = new OnClick(() => {
      if (this.getAmount() == 900) {
        this.amount.positionX = -122
      }
      this.setAmount(this.getAmount() + 100)
    })

    const downButton = new UIImage(this.mainUI, imageTexture)
    downButton.sourceLeft = 260
    downButton.sourceTop = 278
    downButton.sourceWidth = 72
    downButton.sourceHeight = 72
    downButton.width = 30
    downButton.height = 16
    downButton.positionX = -80
    downButton.positionY = 30
    downButton.onClick = new OnClick(() => {
      if (this.getAmount() == 1000) {
        this.amount.positionX = -105
      }
      if (this.getAmount() > 100) {
        this.setAmount(this.getAmount() - 100)
      }
    })

    const exitButton = new UIImage(this.mainUI, imageTexture)
    exitButton.sourceLeft = 346
    exitButton.sourceTop = 128
    exitButton.sourceWidth = 128
    exitButton.sourceHeight = 128
    exitButton.positionX = -65
    exitButton.positionY = -33
    exitButton.onClick = new OnClick(() => {
      this.showUI(Type.None)
    })

    this.startButton = new UIImage(this.mainUI, imageTexture)
    this.startButton.sourceLeft = 183
    this.startButton.sourceTop = 128
    this.startButton.sourceWidth = 128
    this.startButton.sourceHeight = 128
    this.startButton.positionX = 65
    this.startButton.positionY = -33

    this.donated = new UIText(this.mainUI)
    this.donated.value = ''
    this.donated.fontSize = 30
    this.donated.positionX = -150
    this.donated.positionY = -70

    const label2 = new UIText(this.mainUI)
    label2.value = 'MANA donated so far'
    label2.fontSize = 30
    label2.positionX = -30
    label2.positionY = -70
  }
  showUI(ui: Type) {
    if (ui === Type.None) {
      this.mainUI.visible = false
      this.poolUI.visible = false
      this.messageUI.visible = false
    }
    else if (ui === Type.Main) {
      this.mainUI.visible = true
      this.poolUI.visible = false
      this.messageUI.visible = false
    }
    else if (ui === Type.Pool) {
      this.mainUI.visible = false
      this.poolUI.visible = true
      this.messageUI.visible = false
    }
    else if (ui === Type.Message) {
      this.mainUI.visible = false
      this.poolUI.visible = false
      this.messageUI.visible = true
    }
    this.active = ui
  }
  onStartClick(fx: () => void) {
    this.startButton.onClick = new OnClick(fx)
  }
  showMessage(msg: string) {
    this.messageText.value = msg
    this.showUI(Type.Message)
  }
  getAmount() {
    return Number(this.amount.value)
  }
  private setAmount(val: number) {
    this.amount.value = val.toString()
  }
  setDonated(val: number) {
    this.donated.value = val.toString()
  }
  updatePoolProgress(): boolean {
    if (this.poolProgressCounter + 1 < progressBar.length) {
      this.poolProgressCounter += 1
      this.poolProgress.sourceLeft = progressBar[this.poolProgressCounter][0]
      this.poolProgress.sourceTop = progressBar[this.poolProgressCounter][1]
      return true
    }
    else {
      return false
    }
  }
  resetPoolProgress() {
    this.poolProgressCounter = 0
    this.poolProgress.sourceLeft = progressBar[this.poolProgressCounter][0]
    this.poolProgress.sourceTop = progressBar[this.poolProgressCounter][1]
  }
}
