export class Timers {
  private timers: {event: Object, sec: number}[] = []
  constructor() {}
  exist(cls?): boolean {
    if (cls) {
      return this.timers.some((timer) => {
        return timer.event instanceof cls
      })
    }
    else {
      return this.timers.length > 0
    }
  }
  addTimer(event: Object, sec: number) {
    this.timers.push({event: event, sec: sec})
  }
  updateTimers(): Array<Object> {
    let finished = []
    let timersLeft = []
    this.timers.forEach((timer) => {
      timer.sec -= 0.5
      if (timer.sec == 0) {
        finished.push(timer.event)
      }
      else {
        timersLeft.push(timer)
      }
    })
    this.timers = timersLeft
    return finished
  }
  removeTimers(cls) {
    this.timers = this.timers.filter((timer) => {
      return !(timer.event instanceof cls)
    })
  }
}
