export const places = [
  {
    pos: new Vector3(19.75, 8.12, 21.3),
    rot: Quaternion.Euler(0, 290, 0),
    scareDistance: 8 
  },
  {
    pos: new Vector3(12.14, 3.42, 24.21),
    rot: Quaternion.Euler(0, 160, 0),
    scareDistance: 5 
  },
  {
    pos: new Vector3(12.92, 4.85, 10.75),
    rot: Quaternion.Euler(0, 20, 0),
    scareDistance: 6 
  }
]

export const routes = [
  [
    places[0].rot,                // 0 => 1
    Quaternion.Euler(0, 210, 0)   // 0 => 2
  ],
  [
    Quaternion.Euler(0, 110, 0),  // 1 => 0
    Quaternion.Euler(0, 176, 0)   // 1 => 2
  ],
  [
    Quaternion.Euler(0, 30, 0),   // 2 => 0
    Quaternion.Euler(0, 356, 0)   // 2 => 1
  ]
]

export const crows = [
  new AudioClip('sounds/crow1.mp3'),
  new AudioClip('sounds/crow2.mp3')
]
