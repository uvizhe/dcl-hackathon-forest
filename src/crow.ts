import { places, routes, crows } from 'crowMotion'

@Component("crowPosition")
export class CrowPosition {
  travelCount: number = 0
  placeId: number = 0
  nextPlaceId: number = 0
  travelRotId: number = 0
  isMoving: boolean = false
  isPivoting: boolean = false
  isAiming: boolean = false
  aimFraction: number = 0
  moveFraction: number = 0
  pivotFraction: number = 0
}

enum AnimState {
  Idle,
  Takeoff,
  Fly,
  Glide,
  Land
}

export class Crow extends Entity {
  static readonly AnimState = AnimState
  private animator: Animator
  private clipIdle: AnimationState
  private clipTakeoff: AnimationState
  private clipFly: AnimationState
  private clipGlide: AnimationState
  private clipLand: AnimationState
  private animState: AnimState
  constructor() {
    super()
    this.addComponent(new GLTFShape("models/crow/crow.glb"))
    this.animator = new Animator()
    this.addComponent(this.animator)
    this.clipIdle = new AnimationState("idle1")
    this.clipTakeoff = new AnimationState("takeoff")
    this.clipFly = new AnimationState("fly")
    this.clipGlide = new AnimationState("glide")
    this.clipLand = new AnimationState("land")
    this.clipIdle.looping = false
    this.clipTakeoff.looping = false
    this.clipTakeoff.speed = 1.5
    this.clipLand.looping = false
    this.clipLand.speed = 0.5
    this.animator.addClip(this.clipIdle)
    this.animator.addClip(this.clipTakeoff)
    this.animator.addClip(this.clipFly)
    this.animator.addClip(this.clipGlide)
    this.animator.addClip(this.clipLand)
    this.addComponent(new CrowPosition())
    this.addComponent(new Transform({
      position: places[0].pos,
      rotation: places[0].rot,
      scale: new Vector3(0.03, 0.03, 0.03)
    }))
    this.addComponent(new OnClick(() => {
      this.getComponent(CrowPosition).isAiming = true
    }))
    this.clipIdle.play()
    this.animState = AnimState.Idle
  }
  private crow() {
    let chance
    if (this.getComponent(CrowPosition).travelCount == 0) {
      // crow at the first fly
      chance = 0
    }
    else {
      chance = Math.floor(Math.random() * 3)
    }
    if (chance < 2) {
      const crowSource = new AudioSource(crows[chance])
      this.addComponentOrReplace(crowSource)
      crowSource.playOnce()
    }
  }
  private stopAnimation() {
    if (this.animState == AnimState.Idle) {
      this.clipIdle.stop()
    }
    else if (this.animState == AnimState.Takeoff) {
      this.clipTakeoff.stop()
    }
    else if (this.animState == AnimState.Fly) {
      this.clipFly.stop()
    }
    else if (this.animState == AnimState.Glide) {
      this.clipGlide.stop()
    }
    else if (this.animState == AnimState.Land) {
      this.clipLand.stop()
    }
  }
  idle() {
    this.stopAnimation()
    this.clipIdle.play()
    this.animState = AnimState.Idle
  }
  takeoff() {
    this.stopAnimation()
    this.clipTakeoff.play()
    this.animState = AnimState.Takeoff
  }
  fly() {
    this.crow()
    this.stopAnimation()
    this.clipFly.play()
    this.animState = AnimState.Fly
  }
  glide() {
    this.stopAnimation()
    this.clipGlide.play()
    this.animState = AnimState.Glide
  }
  land() {
    this.stopAnimation()
    this.clipLand.play()
    this.animState = AnimState.Land
  }
  move(dt, camera) {
    let crowPos = this.getComponent(CrowPosition)
    let transform = this.getComponent(Transform)
    if (crowPos.isAiming || crowPos.isMoving || crowPos.isPivoting) {
      this.crowMoveSimple(dt, crowPos, transform)
    }
    else {
      let crowDistance = Vector3.Distance(
        camera.position, places[crowPos.placeId].pos)
      if (crowPos.travelCount == 0) {
        crowDistance = crowDistance / 2
      }
      if (crowDistance < places[crowPos.placeId].scareDistance) {
        crowPos.isAiming = true
      }
    }
  }
  private crowMoveSimple(dt, crowPos, transform) {
    if (crowPos.isAiming) {
      if (crowPos.placeId == crowPos.nextPlaceId) {
        if (crowPos.travelCount < 2) {
          // Two first routes are predefined
          crowPos.nextPlaceId = crowPos.travelCount + 1
          crowPos.travelRotId = crowPos.travelCount
        }
        else {
          // Randomly choose next route
          const draw = Math.floor(Math.random() * 2)
          let destinations = [0, 1, 2].filter(x => crowPos.placeId != x)
          crowPos.nextPlaceId = destinations[draw]
          crowPos.travelRotId = draw
        }
      }
      if (this.animState !== AnimState.Takeoff) {
        this.takeoff()
      }
      if (crowPos.aimFraction < 1) {
        let rot = Quaternion.Slerp(
          places[crowPos.placeId].rot,
          routes[crowPos.placeId][crowPos.travelRotId],
          crowPos.aimFraction)
        transform.rotation = rot
        crowPos.aimFraction += dt / 0.5
        if (crowPos.aimFraction >= 1) {
          crowPos.aimFraction = 0
          crowPos.isAiming = false
          crowPos.isMoving = true
        }
      }
    }
    else if (crowPos.isMoving) {
      if (this.animState !== AnimState.Fly) {
        this.fly()
      }
      if (crowPos.moveFraction < 1) {
        transform.position = Vector3.Lerp(
          places[crowPos.placeId].pos,
          places[crowPos.nextPlaceId].pos,
          crowPos.moveFraction
        )
        crowPos.moveFraction += dt / 1
        if (crowPos.moveFraction >= 1) {
          crowPos.moveFraction = 0
          crowPos.isMoving = false
          crowPos.isPivoting = true
        }
      }
    }
    else if (crowPos.isPivoting) {
      if (this.animState !== AnimState.Land) {
        this.land()
      }
      if (crowPos.pivotFraction < 1) {
        let rot = Quaternion.Slerp(
          routes[crowPos.placeId][crowPos.travelRotId],
          places[crowPos.nextPlaceId].rot,
          crowPos.pivotFraction)
        transform.rotation = rot
        crowPos.pivotFraction += dt / 1
        if (crowPos.pivotFraction >= 1) {
          crowPos.pivotFraction = 0
          crowPos.isPivoting = false
          crowPos.travelCount += 1
          crowPos.placeId = crowPos.nextPlaceId
          //crowPos.isAiming = true // perpetuum mobile
          this.idle()
        }
      }
    }
  }
}
