export class Stone extends Entity {
  private glow: boolean = false
  private glowMore: boolean = true
  private glowAway: boolean = false
  private stoneIndicator: Entity
  private gazePoints = [
    new Vector3(11.897, 1.90, 23.535),
    new Vector3(12.885, 1.90, 23.635),
    new Vector3(11.325, 1.90, 22.435)
  ]
  private gazeAngles = [
    [ [-20, 70], [-50, 10], [-20, 70] ],
    [ [-50, 80], [-70, 0], [-20, 90] ],
    [ [-15, 60], [-35, 25], [-25, 25] ]
  ]
  private material: Material
  constructor() {
    super()
    this.addComponent(new GLTFShape("models/stone/stone.gltf"))
    this.addComponent(new Transform({
      position: new Vector3(12.2, -0.1, 23),
      rotation: Quaternion.Euler(280, -10, 180),
      scale: new Vector3(20, 20, 20)
    }))

    const stoneCollider = new Entity()
    stoneCollider.addComponent(
      new GLTFShape("models/stone/collider.glb"))
    stoneCollider.addComponent(new Transform({
      position: new Vector3(11.83, 1, 23.4),
      rotation: Quaternion.Euler(-1, 67.2, 14),
      scale: new Vector3(0.5, 0.5, 0.5)
    }))
    engine.addEntity(stoneCollider)

    this.stoneIndicator = new Entity()
    this.stoneIndicator.addComponent(new Transform({
      position: new Vector3(11.897, 1.90, 23.535),
      rotation: Quaternion.Euler(14.15, -22.2, 10.8),
      scale: new Vector3(1.48, 1.56, 0.3)
    }))
    this.material = new Material()
    this.material.transparencyMode = 2
    this.material.albedoTexture = new Texture("models/stone/halo.png")
    this.material.emissiveColor = Color3.FromHexString("#A1FF1A")
    this.material.emissiveIntensity = 0
    this.stoneIndicator.addComponent(this.material)
    const indicatorBox = new BoxShape()
    this.stoneIndicator.addComponent(indicatorBox)
    engine.addEntity(this.stoneIndicator)
  }
  onClick(fx: (e) => void) {
    this.addComponent(new OnClick(e => { fx(e) }))
  }
  inSight(camera: Camera) {
    let gaze = camera.rotation.eulerAngles
    for (let i = 0; i < this.gazePoints.length; i++) {
      let distance = Vector3.Distance(camera.position, this.gazePoints[i])
      if (distance <= 2 &&
        ((gaze.x > this.gazeAngles[i][0][0] &&
          gaze.x < this.gazeAngles[i][0][1]) &&
         (gaze.y > this.gazeAngles[i][1][0] &&
          gaze.y < this.gazeAngles[i][1][1]) &&
         (gaze.z > this.gazeAngles[i][2][0] &&
          gaze.z < this.gazeAngles[i][2][1]))) {
        return true
      }
    }
    return false
  }
  isGlowing() {
    return this.glow
  }
  glowStart() {
    this.stoneIndicator.getComponent(Transform).scale.z = 0.3
    this.glow = true
  }
  glowStep(fraction: number) {
    if (this.glowMore) {
      this.material.emissiveIntensity += 9 * fraction
      if (this.material.emissiveIntensity >= 7) {
        this.glowMore = false
      }
    }
    else {
      this.material.emissiveIntensity -= 9 * fraction
      if (! this.glowAway && this.material.emissiveIntensity <= 3) {
        this.glowMore = true
      }
      else if (this.material.emissiveIntensity <= 0) {
        this.glow = false
        this.glowAway = false
        this.material.emissiveIntensity = 0
        this.stoneIndicator.getComponent(Transform).scale.z = 0.1
      }
    }
  }
  glowStop() {
    this.glowMore = false
    this.glowAway = true
  }
}
