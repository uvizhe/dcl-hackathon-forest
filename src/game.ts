import { getProvider } from '@decentraland/web3-provider'
import { getUserAccount } from '@decentraland/EthereumController'
import * as EthConnect from '../node_modules/eth-connect/esm'
import { abiRopsten, abiMain } from '../contracts/mana'
import { UI } from 'ui'
import { Timers } from 'timers'
import {
  manaAddrRopsten, manaAddrMain, donateAddrRopsten, donateAddrMain
} from 'constants'
import { loadTrees } from 'trees'
import { Stone } from 'stone'
import { Crow } from 'crow'
import { Credits } from 'credits'

let ui = new UI()
let timers = new Timers()

const camera = Camera.instance

let stone = new Stone()
engine.addEntity(stone)

let crow = new Crow()
engine.addEntity(crow)

let terrain = new Entity()
terrain.addComponent(new GLTFShape("models/terrain/terrain_colliders2.80.gltf"))
terrain.addComponent(new Transform({
  position: new Vector3(16, 0, 16),
  rotation: Quaternion.Euler(0, 180, 0)
}))
engine.addEntity(terrain)

const credits = new Credits()
engine.addEntity(credits)

loadTrees()

const ambiance = new Entity()
ambiance.addComponent(new Transform({
  position: new Vector3(16, 30, 16)
}))
const audioClip = new AudioClip('sounds/196005__bolland__forest-ambience.mp3')
const audioSource = new AudioSource(audioClip)
ambiance.addComponent(audioSource)
engine.addEntity(ambiance)
audioSource.playOnce()

let requestManager, network, contract, decimals, abi, manaAddr, donateAddr
executeTask(async () => {
  try {
    requestManager = await getRequestManager()
    network = await requestManager.net_version()
    if (Number(network) === 1) {
      abi = abiMain
      manaAddr = manaAddrMain
      donateAddr = donateAddrMain
    }
    else {
      abi = abiRopsten
      manaAddr = manaAddrRopsten
      donateAddr = donateAddrRopsten
    }
    contract = await getContract(requestManager)
    decimals = await contract.decimals()
  } catch (error) {
    log(error.toString())
  }
})

const events = new EventManager()
@EventConstructor()
class TXCheckEvent {
  constructor(public txHash: string) {}
}
@EventConstructor()
class PoolProgressEvent {
  constructor() {}
}
@EventConstructor()
class PoolResetEvent {
  constructor() {}
}
@EventConstructor()
class MessageCloseEvent {
  constructor() {}
}
@EventConstructor()
class SoundRestartEvent {
  constructor() {}
}
@EventConstructor()
class PoolSoundRestartEvent {
  constructor() {}
}
timers.addTimer(new SoundRestartEvent(), 59)

const switchUI = () => {
  if (ui.active === UI.Type.Main) {
    ui.showUI(UI.Type.None)
    gazeAtStone = false
  }
  else {
    executeTask(async () => {
      await updateForestBalance()
    })
    ui.showUI(UI.Type.Main)
  }
}
stone.onClick(switchUI)

const getRequestManager = async () => {
  const provider = await getProvider()
  const requestManager = new EthConnect.RequestManager(provider)
  return requestManager
}

const getContract = async (requestManager) => {
  const factory = new EthConnect.ContractFactory(requestManager, abi)
  const contract = (await factory.at(manaAddr)) as any
  return contract
}

const updateForestBalance = async () => {
  const balance = await contract.balanceOf(donateAddr)
  ui.setDonated(balance.div(10**decimals))
}

const sendMana = async (contract, value) => {
  const address = await getUserAccount()
  const tx = await contract.transfer(
    donateAddr,
    value*(10**decimals),
    { from: address }
  )
  return tx
}

const donateMana = async () => {
  try {
    let txHash = await sendMana(contract, ui.getAmount())
    ui.showUI(UI.Type.None)
    timers.addTimer(new TXCheckEvent(txHash), 1)
    stone.glowStart()
  } catch (error) {
    log(error.toString())
  }
}
ui.onStartClick(donateMana)

const checkDonationResult = async (txHash) => {
  try {
    const res = await requestManager.eth_getTransactionReceipt(txHash)
    return res
  } catch (error) {
    log(error.toString())
    return { status: 0 }
  }
}

// Systems

let counter = 0
class TimerSystem {
  update(dt: number) {
    if (timers.exist()) {
      counter += dt * 2
      if (counter >= 1) {
        counter = 0
        const eventsToFire = timers.updateTimers()
        eventsToFire.forEach((event) => {
          events.fireEvent(event)
        })
      }
    }
  }
}
engine.addSystem(new TimerSystem())

let glowCounter = 0
let gazeAtStone = false
class StoneSystem {
  update(dt: number) {
    // Processing glow effect on every update
    // makes event listeners execute too late :(
    glowCounter += dt * 8
    if (glowCounter >= 1 && stone.isGlowing()) {
      stone.glowStep(dt)
      glowCounter = 0
    }
    if (stone.inSight(camera)) {
      if (! gazeAtStone && ui.active === UI.Type.None) {
        executeTask(async () => {
          await updateForestBalance()
        })
        ui.showUI(UI.Type.Main)
        gazeAtStone = true
      }
    }
    else {
      if (gazeAtStone && ui.active === UI.Type.Main) {
        ui.showUI(UI.Type.None)
        gazeAtStone = false
      }
    }
  }
}
engine.addSystem(new StoneSystem())

class CrowMoveSystem {
  update(dt: number) {
    crow.move(dt, camera)
  }
}
engine.addSystem(new CrowMoveSystem())

const pool = new Entity()
pool.addComponent(new Transform({
  position: new Vector3(22.4, -4, 6.7)
}))
const poolClip = new AudioClip('sounds/pond.mp3')
const poolSource = new AudioSource(poolClip)
pool.addComponent(poolSource)
engine.addEntity(pool)
let poolIsActive = true
class PoolSystem {
  update() {
    if (poolIsActive) {
      executeTask(async () => {
        let poolDistance = Vector3.Distance(
          camera.position, pool.getComponent(Transform).position)
        if (poolDistance <= 5) {
          if (! timers.exist(PoolSoundRestartEvent)) {
            poolSource.playOnce()
            timers.addTimer(new PoolSoundRestartEvent(), 17)
          }
          if (ui.active === UI.Type.None) {
            // Get to the pool and no distractions
            ui.showUI(UI.Type.Pool)
            timers.addTimer(new PoolProgressEvent(), 1.5)
          }
        }
        else if (poolDistance > 5) {
          poolSource.playing = false
          timers.removeTimers(PoolSoundRestartEvent)
          if (ui.active === UI.Type.Pool) {
            // Get out the pool while pool progress
            ui.showUI(UI.Type.None)
            timers.removeTimers(PoolProgressEvent)
            ui.resetPoolProgress()
          }
        }
      })
    }
  }
}
engine.addSystem(new PoolSystem())
// Event listeners

events.addListener(TXCheckEvent, null, ({ txHash }) => {
  executeTask(async () => {
    const txReceipt = await checkDonationResult(txHash)
    if (txReceipt) {
      if (txReceipt.status == 1) {
        stone.glowStop()
        ui.showMessage('Your donation has been received. Thank you!')
        timers.addTimer(new MessageCloseEvent(), 4)
      }
      else {
        stone.glowStop()
        ui.showMessage('Your donation has failed. Hmm...')
        timers.addTimer(new MessageCloseEvent(), 4)
      }
    }
    else {
      timers.addTimer(new TXCheckEvent(txHash), 1)
    }
  })
})

events.addListener(PoolProgressEvent, null, () => {
  if (ui.updatePoolProgress()) {
    timers.addTimer(new PoolProgressEvent(), 1.5)
  }
  else {
    timers.removeTimers(PoolProgressEvent)
    ui.showMessage('                      You feel refreshed')
    timers.addTimer(new MessageCloseEvent(), 3)
    poolIsActive = false
    timers.addTimer(new PoolResetEvent(), 30)
  }
})

events.addListener(PoolResetEvent, null, () => {
  ui.resetPoolProgress()
  poolIsActive = true
})

events.addListener(MessageCloseEvent, null, () => {
  ui.showUI(UI.Type.None)
})

events.addListener(SoundRestartEvent, null, () => {
  audioSource.playOnce()
  timers.addTimer(new SoundRestartEvent(), 59)
})

events.addListener(PoolSoundRestartEvent, null, () => {
})
